---
author: Nicola Paolucci
title: "What You Need To Know About The New Git 1.8.3"
date: 2014-09-03
---

Whether you use `git` on the command line or via a tool of choice like [SourceTree][9]; whether you host your code on [Bitbucket Cloud][12] or on [Stash][15] (now called Bitbucket Server) behind your company firewall, if you're like me - *wink* - when a new `git` release comes out it's always a party.

## Smooth Upgrade Path For Gitters

The new [git][4] [1.8.3][1] release is out. Of course this means upgrading to the latest version. It should be relatively pain free: 

- Just type `brew update && brew upgrade git` if you use [homebrew][5] on OSX (**due to a last minute bug discovered in parsing `.gitignore` on OSX, homebrew hasn't distributed the upgrade yet**).
- Use a magic [apt trick][6] if on [Ubuntu][14] based distributions *(I leave other distributions as an exercise for the reader)*.
- Simply run the new [installer][8] if on Windows *(the Windows port sometimes lags a bit behind so please be patient if you can't find it right away)*.

For previous notes on `git` 1.8.2 see [here][7].

## So what's new in this release?

As usual [lots][1] has been fixed, it seems to me that this drop packs a lot more than the previous cycle. Without further ado, let me then show you a few things that caught my interest.

### Colors, Colors And More Polish Everyone

Refinement of the command line UI continues with a few neat updates:

- During a `git rebase` session the prompt string generator (in `contrib/completion/`) will show how many changes there are in total and how many have been replayed.

- `git branch -v -v` can now paint the name of the branch it integrates with in a different color (color.branch.upstream, defaulting to blue):

```
npaolucci:~/dev/projects/stash] master ± git branch -v -v
  1.3           3a7ec9d [origin/1.3] Merge pull request #765 from STASH-2808-...
  gmail-renders 128fe79 [origin/gmail-renders] made pattern DOT_IN_WORD...
* master        66865b0 [origin/master] Automatic merge from 2.4 -> master
```

- `git log --format` now sports a `%C(auto)` token that tells Git to use color when resolving `%d` (decoration), `%h` (short commit object name), etc. for terminal output.

- `git count-objects` gained a `--human-readable` and `-H` option to show various large numbers in Ki/Mi/GiB scaled as necessary:

```
[npaolucci:~/dev/projects/stash] master ± git count-objects -H
169 objects, 680.00 KiB
```

### Help Shows List Of Guides

`git help -g` is a new flag that will list the guides available, just like list of commands are given with `-a`:

```
[npaolucci:~/dev/projects/stash] master ± git help -g
The common Git guides are:

  attributes   Defining attributes per path
  glossary     A Git glossary
  ignore       Specifies intentionally untracked files to ignore
  modules      Defining submodule properties
  revisions    Specifying revisions and ranges for Git
  tutorial     A tutorial introduction to Git (for version 1.5.1 or newer)
  workflows    An overview of recommended workflows with Git

'git help -a' and 'git help -g' lists available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.
```

### Better "Triangular" Work Flow Support

`git` now supports a "*triangular*" work flow for situations where your default process is to always pull from one place and always push to a different one. To accomplish it you can now override configuration variable `origin` with a new `remote.pushdefault` and override `branch.❊.remote` with `branch.❊.pushremote`.

### Improved Status For Reverts, Rebases and Bisects

`git status` has been improved to report when you are in the middle of a `revert` session, a `cherry-pick` or a `bisect` session.

### Un-tracking Submodules! Finally.

If you've read my earlier [article on `git submodule`][10] you have seen that relatively simple operations were not supported too well by the `submodule` command line interface. Well there is good news!

Now you can say *"I no longer am interested in this submodule"* with `git submodule deinit`.

### Other Nice Things Worth Mentioning

- The new `--follow-tags` option tells `git push` to push relevant annotated tags when pushing branches out.

- There was no good way to ask "I have a random string that came from outside world. I want to turn it into a 40-hex object name while making sure such an object exists".  A new peeling suffix `^{object}` can be used for that purpose, together with `rev-parse --verify`.

## Conclusions

That's it for now, enjoy the goodies and let me know if there are worthy items I missed in the long list of updates.

As usual follow me [@durdn][2] and the awesome [@AtlDevtools][3] team for more DVCS rocking.

[1]: https://github.com/git/git/blob/master/Documentation/RelNotes/1.8.3.txt
[2]: http://twitter.com/durdn
[3]: http://twitter.com/AtlDevTools
[4]: http://git-scm.com/
[5]: http://mxcl.github.com/homebrew/
[6]: https://launchpad.net/~git-core/+archive/ppa
[7]: https://blogs.atlassian.com/2013/03/highlights-new-git-1-8-2/
[8]: http://msysgit.github.com/
[9]: http://www.sourcetreeapp.com/
[10]: https://blogs.atlassian.com/2013/03/git-submodules-workflows-tips/
[11]: http://durdn.com/blog/2012/11/22/must-have-git-aliases-advanced-examples/
[12]: https://bitbucket.org
[13]: http://www.atlassian.com/en/software/stash/whats-new/stash-24
[14]: http://www.ubuntu.com
[15]: http://www.atlassian.com/software/stash/overview

