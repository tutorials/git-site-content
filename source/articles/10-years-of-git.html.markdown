---
author: Cody Rushing
title: "10 Years of Git"
date: 2015-04-03
---
10 years ago Linus Torvalds started writing code for a new distributed version control system on a Sunday and only a mere few days later, the world was given the gift of Git.
