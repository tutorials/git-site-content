---
author: Nicola Paolucci
title: "What You Need To Know About The New Git 1.8.2 Release"
date: 2014-09-03
---

I love scouring the release notes of my favorite tools for hidden (or not so hidden) gems. It's a little bit like Christmas every time. I get that nice feeling of anticipation and curiosity when new versions are released of my faithful OSX open source window manager [Slate][1], on [Rails][2], [Django][3], [CoffeeScript][4] and of course [Git][5] and many others.

## Upgrading, upgrading, upgrading

So the new [Git][5] [1.8.2][6] release is out. Of course this means upgrading to the latest version. It should be relatively pain free:

*   Just type `brew update && brew upgrade git` if you use [homebrew][7] on OSX.
*   Use a magic [apt trick][8] if on [Ubuntu][1] based distributions (I leave other distributions as an exercise for the reader).
*   Simply run the new [installer][9] if on Windows.

## So what's new in Git 1.8.2?

As usual, [a lot][6] has been fixed. A few specific things caught my interest in this latest release.

### Streamlined behavior for `add -u` and `-A`

`git add -u` is the flag used to tell Git to also stage deletions when adding stuff from the current directory to the index.

And `git add -A` is used to add everything and stage deletions starting from the current directory. It used to be equivalent to:

```
git add .; git add -u
```

This behavior has now changed and `-u/-A` flags will operate on the entire source tree to make it consistent with `git commit -a` and others that already work this way. This is a _backwards incompatible change_, though hopefully of light impact.

### Useful `**/` pattern to use in `.gitignore`

The beloved `.gitignore` file gains a cool new `**/` pattern that matches zero or more levels of a subdirectory:
E.g. "foo/**/bar" matches "bar" in "foo" itself or in a
subdirectory of "foo".

In addition to this, Git now sports a `check-ignore` command that can be used to debug the `.gitignore` settings:
E.g.

```
[4967] λ # git check-ignore --verbose dist/
.gitignore:1:dist       dist/
```

### Improved completion script

If you use the command line and have setup [git shell auto completion][10] you have probably noticed how useful it is. In this release the completion script has been improved to be smart about which files to show when you [`git add`][11], discarding not relevant ones like unmodified paths.

### Colored Aliases And Script Interactions

I use heavily colored `log` commands as I showed in my [alias post][12] a while ago. This release brings in a nice tweak that automatically disables colors when the output is not used for a terminal, very useful when using [Git commands][13] in scripts.

How do you use it? Just pre-pend `auto` to the color specifier like:

```
%C(auto,blue)Hello%C(auto,reset)
```

### Ancestry graph in `git log` now works with combined diff output

The `--graph` output of git log used to struggle if you added the `-p` to show the combined diff output. Now it works as expected:

```
git log -p --cc --graph
```

### Better Sync Status Between Local and Remote Branches

This is actually something from the [1.8.1][14] release but neat nonetheless:
> When "[git checkout][15]" checks out a branch, it tells the user how far
> behind (or ahead) the new branch is relative to the remote tracking
> branch it builds upon. The message now also advises how to sync
> them up by pushing or pulling. This can be disabled with the
> advice.statusHints configuration variable.

## Conclusions

That's it for now. Enjoy the goodies, let me know what you think on Twitter [@AtlDevTools][16].

[1]: https://github.com/jigish/slate
[2]: http://rubyonrails.org/
[3]: https://www.djangoproject.com/
[4]: http://coffeescript.org/
[5]: http://git-scm.com/
[6]: https://github.com/git/git/blob/master/Documentation/RelNotes/1.8.2.txt
[7]: http://mxcl.github.com/homebrew/
[8]: https://launchpad.net/~git-core/+archive/ppa
[9]: http://msysgit.github.com/
[10]: http://git-scm.com/book/en/Git-Basics-Tips-and-Tricks#Auto-Completion
[11]: /tutorials/saving-changes/git-add
[12]: http://durdn.com/blog/2012/11/22/must-have-git-aliases-advanced-examples/
[13]: /git/tutorials
[14]: https://github.com/git/git/blob/master/Documentation/RelNotes/1.8.1.txt
[15]: /tutorials/using-branches/git-checkout
[16]: https://twitter.com/AtlDevTools