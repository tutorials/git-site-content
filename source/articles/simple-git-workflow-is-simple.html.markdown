---
author: Nicola Paolucci
title: "Simple Git workflow is simple"
date: 2014-09-03
---

Many teams have already migrated to `git` and many more are transitioning to it now. Apart from training single developers and appointing **Champions** to help with the adoption it is imperative to pick a nice and simple code collaboration practice that does not complicate things too much. With `git` one can definitely conjure very complicated workflows, I've seen them first hand.

A manual on workflows does not come pre-installed with `git`, but maybe it should seeing how many people have questions on the topic. The good news is that we're working hard to write material that helps.

## Recent webinars and guides on workflows

* [Git Branching for Agile Teams](https://www.youtube.com/watch?v=9SZ7kSQ2424)
* [Git Ready: Workflows Webinar, Atlassian, September 2013](https://www.youtube.com/watch?v=B78AdLNZBQQ)

If you prefer reading and pretty pictures, one of the most popular sections of our `git` tutorial site is [the *workflows* section][9].

But before you leave for those destinations please read on, because I have something really cool for you.

I want to detail a terse but complete description of a simple workflow for *continuous delivery*. The prerequisite is that you and your team are at least a little bit acquainted with `git`, and have good knowledge of the `rebase` command in the two forms (interactive and not).

##A basic basic branching workflow for continuous delivery

The simple workflow I want to describe has two guiding principles:

- `master` is always production-like and deployable.
- `rebase` during feature development, explicit (non fast-forward) `merge` when done.

Pulling change-sets using `rebase` rewrites the history of the branch you're working on and keeps your changes on top.

![rebase-on-feature][10]

The `rebase` you want in this workflow is the one in the second picture.

Armed with these guiding principles let's breakdown the seven steps:

### 1. Start by pulling down the latest changes from `master`

This is done easily with the common `git` commands:

```
git checkout master
git fetch origin
git merge master
```

I like to be more explicit and use `fetch/merge` but the two commands are equivalent to: `git pull origin master`.

### 2. Branch off to isolate the feature or bug-fix work in a branch

Now create a branch for the feature or bug-fix:

```
git checkout -b PRJ-123-awesome-feature
```

The branch name structure I show here is just the one we use, but you can pick any convention you feel comfortable with.

### 3. Now you can work on the feature

Work on the feature as long as needed. Make sure your commits are meaningful and do not cluster separate changes together.

### 4. To keep your feature branch fresh and up to date with the latest changes in master, use `rebase`

Every once in a while during the development update the feature branch with the latest changes in `master`. You can do this with:

```
git fetch origin
git rebase origin/master
```

In the (somewhat less common) case where other people are also working on the same shared remote feature branch, also `rebase` changes coming from it:

```
git rebase origin/PRJ-123-awesome-feature
```

At this point solve any conflicts that come out of the rebase.

Resolving conflicts during the `rebase` allows you to have always clean merges at the end of the feature development. It also keeps your feature branch history clean and focused without spurious noise.

### 5. When ready for feedback push your branch remotely and create a pull request

When it's time to share your work and solicit feedback you can push your branch remotely with:

```
git push -u origin PRJ-123-awesome-feature
```

(if the branch is already set as 'upstream' and your remote is called 'origin', 'git push' is enough)

Now you can create a pull request on your favorite `git` server (for example [Bitbucket Server][6] or [Bitbucket Cloud][7]).

After the initial push you can keep pushing updates to the remote branch multiple times throughout. This can happen in response to feedback, or because you're not done with the development of the feature.

### 6. Perform a final `rebase` cleanup after the pull request has been approved

After the review is done, it's good to perform a final cleanup and scrub of the feature branch commit history to remove spurious commits that are not providing relevant information. In some cases – if your team is experienced and they can handle it – you can `rebase` also during development, but I **strongly **advise against it.:

```
git rebase -i origin/master
```

(At this point if you have rewritten the history of a published branch and provided that no one else will commit to it or use it, you might need to push your changes using the `--force` flag).

### 7. When development is complete record an explicit merge

When finished with the development of the feature branch and reviewers have reviewed your work, `merge` using the flag `--no-ff`. This will preserve the context of the work and will make it easy to revert the whole feature if needed. Here are the commands:

```
git checkout master
git pull origin master

git merge --no-ff PRJ-123-awesome-feature
```

If you followed the advice above and you have used `rebase` to keep your feature branch up to date, the actual merge commit will not include any changes; this is cool! The merge commit becomes just a marker that stores the context about the feature branch.

For more information have a look at my recent article on [the pros and cons of enforcing a merge vs rebase workflow][5].

### Useful `.gitconfig` option to toggle:

You can instruct `git` so that any `pull` uses `rebase` instead than `merge` and it preserves while doing so:

```
git config --global branch.autosetuprebase always 
git config --global pull.rebase preserve #(this is a very recent and useful addition that appeared in git 1.8.5)
```

Not everyone likes to change the default behavior of core commands so you should only incorporate the above if you understand its implications. See [Stack Overflow for details on preserve merges][1].

## Conclusions

This should give you plenty of material to get acquainted with workflows, branching models and code collaboration possibilities. For more `git` rocking follow me [@durdn][2] and the awesome [@AtlDevtools][3] team. **Credits:** Inspiration for this post comes partially from this [concise and well made gist][4].

[1]: http://stackoverflow.com/questions/15915430/what-exactly-does-gits-rebase-preserve-merges-do-and-why
[2]: http://twitter.com/durdn
[3]: http://twitter.com/AtlDevTools
[4]: https://gist.github.com/jbenet/ee6c9ac48068889b0912
[5]: https://blogs.atlassian.com/2013/10/git-team-workflows-merge-or-rebase/
[6]: https://www.atlassian.com/software/stash/overview
[7]: https://bitbucket.org
[9]: /tutorials/comparing-workflows
[10]: /images/articles/simple-git-workflow-is-simple/rebase-on-feature.gif


