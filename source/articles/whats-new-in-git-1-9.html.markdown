---
author: Nicola Paolucci
title: "What's new in Git 1.9"
date: 2014-09-03
---

I know you are all busy with [looting marathons][1] in [DayZ][2] but bear with me. Something is worth checking out today. The latest `git` point release (`1.9`) has come out!

Here is what caught my eye from the [release notes][3]. If you're inclined you can obviously go ahead and scour the notes in all their nerdy beauty by yourself. But let me help!

Before I start let me also gently point you to my earlier notes on releases [1.8.2][4], [1.8.3][5], [1.8.4][6], [1.8.5][7]. Lots of neat tricks and goodies in there, too.

## Upgrade pointers for everyone

How do you upgrade? Here some instructions that should get you started:

*   Type `brew update && brew upgrade git` if you use [homebrew][8] on OSX.
*   Use a magic [apt trick][10] if on [Ubuntu][9] based distributions _(I leave other distributions as an exercise for the reader)_.
*   Simply run the new [installer][11] if on Windows _(the Windows port sometimes lags a bit behind so please be patient if you can't find it right away)_.

**Note!:** Package managers in some cases might delay the distribution of the update; that is out of my hands.

## Network operations updates: push and fetch

### "git fetch --tags" now fetches all **plus** tags

The meaning of the `--tags` option in `git fetch` has changed; The command now fetches tags **in addition to** what is fetched by the same command without the option.

### Now you can "fetch" from a shallow-clone!

You can now `fetch` from a shallowly-cloned repository, an action that was forbidden before; Note that the receiver becomes now a shallow repository with a truncated history too.

### Standard "git push origin master" has more solid semantics

`git push origin master` or more generally any `git push $where $what` has been enhanced to use the same ref mapping `git push origin` would use to determine which `ref` at the `origin` is to be updated to the local `master`.  For example, with this configuration:

```
[remote "origin"]
push = refs/heads/*:refs/review/*
```

Just like `git push origin` would push out our local branches to corresponding refs under refs/review/ hierarchy at `origin`, `git push origin master` will now update `refs/review/master` over there.

## Logging and aesthetics

### Log everything but directory

The negative _pathspec_ syntax allows `git log ‐‐ . ':!dir'` to tell us _I am interested in everything but the 'dir' directory_.

### difftool progress

`git difftool` shows how many different paths there are in total, and how many of them have been shown so far, to indicate progress.

### Colors and formatting on "for-earch-ref"

`git for-each-ref ‐‐format=...` learned a few formatting directives:

e.g. "%(color:red)%(HEAD)%(color:reset) %(refname:short) %(subject)"

### git log learned "‐‐exclude={glob}", pretty cool!

`git log` and friends learned the `‐‐exclude={glob}` option, to allow people to say _list history of all branches except those that match this pattern_ with `git log ‐‐exclude='_/_' ‐‐branches`.

## Other improvements

### git merge-base ‐‐fork-point to find a fork point out of the reflog

`git merge-base` learned the `‐‐fork-point` mode, that implements the same logic used in `git pull ‐‐rebase` to find a suitable fork point out of the reflog entries for the remote-tracking branch the work has been based on.  `git rebase` has the same logic that can be triggered with the `‐‐fork-point` option.

### Some deprecated commands are gone!

A handful of ancient commands that have long been deprecated are finally gone, namely:

repo-config, tar-tree, lost-found and peek-remote

### Submodule: More robust "git submodule foreach"

The command string given to `git submodule foreach $cmd $args` is passed directly to the shell, without being eval'ed.  This is a backward incompatible change that may break existing users. This differs now from how `ssh` does the parsing. In short it's not needed to over quote the `$args` anymore.

## Conclusions

That's it for now. Let me know if you find this update useful and stay tuned for more. Follow me [@durdn][12] and the awesome [@AtlDevtools][13] team for more DVCS rocking.

[1]: http://heyareyoucool.tumblr.com/
[2]: http://en.wikipedia.org/wiki/DayZ_(video_game)
[3]: https://github.com/git/git/blob/master/Documentation/RelNotes/1.9.0.txt
[4]: http://blogs.atlassian.com/2013/03/highlights-new-git-1-8-2/
[5]: http://blogs.atlassian.com/2013/05/highlights-git-1-8-3/
[6]: http://blogs.atlassian.com/2013/08/what-you-need-to-know-about-the-new-git-1-8-4/
[7]: http://blogs.atlassian.com/2013/12/whats-new-git-1-8-5/
[8]: http://brew.sh/
[9]: http://www.ubuntu.com
[10]: https://launchpad.net/~git-core/+archive/ppa
[11]: http://msysgit.github.com/
[12]: http://twitter.com/durdn
[13]: http://twitter.com/AtlDevTools