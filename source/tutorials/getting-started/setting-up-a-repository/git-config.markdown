The `git config` command lets you configure your Git installation (or an individual repository) from the command line. This command can define everything from user info to preferences to the behavior of a repository. Several common configuration options are listed below. 

### Usage

`git config user.name <name>`

Define the author name to be used for all commits in the current repository. Typically, you’ll want to use the `--global` flag to set configuration options for the current user.

```
git config --global user.name <name>
```

Define the author name to be used for all commits by the current user.

```
git config --global user.email <email>
```

Define the author email to be used for all commits by the current user.

```
git config --global alias.<alias-name> <git-command>
```

Create a shortcut for a Git command.

```
git config --system core.editor <editor>
```

Define the text editor used by commands like git commit for all users on the current machine. The <editor> argument should be the command that launches the desired editor (e.g., vi).

```
git config --global --edit
```

Open the global configuration file in a text editor for manual editing.

### Discussion

All configuration options are stored in plaintext files, so the `git config` command is really just a convenient command-line interface. Typically, you’ll only need to configure a Git installation the first time you start working on a new development machine, and for virtually all cases, you’ll want to use the `--global` flag.

Git stores configuration options in three separate files, which lets you scope options to individual repositories, users, or the entire system:

* `<repo>/.git/config` – Repository-specific settings.
* `~/.gitconfig` – User-specific settings. This is where options set with the `--global` flag are stored.
* `$(prefix)/etc/gitconfig` – System-wide settings. 

When options in these files conflict, local settings override user settings, which override system-wide. If you open any of these files, you’ll see something like the following:

```
[user] 
name = John Smith
email = john@example.com
[alias]
st = status
co = checkout
br = branch
up = rebase
ci = commit
[core]
editor = vim
```

You can manually edit these values to the exact same effect as `git config`.

### Example

The first thing you’ll want to do after installing Git is tell it your name/email and customize some of the default settings. A typical initial configuration might look something like the following:

```
# Tell Git who you are
git config --global user.name "John Smith"
git config --global user.email john@example.com
```

```
# Select your favorite text editor
git config --global core.editor vim
```

```
# Add some SVN-like aliases
git config --global alias.st status
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.up rebase
git config --global alias.ci commit
```

This will produce the `~/.gitconfig` file from the previous section.
