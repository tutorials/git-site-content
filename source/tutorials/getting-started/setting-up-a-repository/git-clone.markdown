The `git clone` command copies an existing Git repository. This is sort of like `svn checkout`, except the “working copy” is a full-fledged Git repository—it has its own history, manages its own files, and is a completely isolated environment from the original repository.

As a convenience, cloning automatically creates a remote connection called origin pointing back to the original repository. This makes it very easy to interact with a central repository.

### Usage

```
git clone <repo>
```

Clone the repository located at `<repo>` onto the local machine. The original repository can be located on the local filesystem or on a remote machine accessible via HTTP or SSH.

```
git clone <repo> <directory>
```

Clone the repository located at `<repo>` into the folder called `<directory>` on the local machine.

### Discussion

If a project has already been set up in a central repository, the `git clone` command is the most common way for users to obtain a development copy. Like [`git init`](/tutorials/setting-up-a-repository/git-init), cloning is generally a one-time operation—once a developer has obtained a working copy, all version control operations and collaborations are managed through their local repository.

#### Repo-To-Repo Collaboration

It’s important to understand that Git’s idea of a “working copy” is very different from the working copy you get by checking out code from an SVN repository. Unlike SVN, Git makes no distinction between the working copy and the central repository—they are all full-fledged Git repositories.

This makes collaborating with Git fundamentally different than with SVN. Whereas SVN depends on the relationship between the central repository and the working copy, Git’s collaboration model is based on repository-to-repository interaction. Instead of checking a working copy into SVN’s central repository, you [push](/tutorials/syncing/git-push) or [pull](/tutorials/syncing/git-pull) commits from one repository to another.

![Git Tutorial: Repo to Working Copy Collaboration](/images/tutorials/getting-started/setting-up-a-repository/03.svg)
![Git Tutorial: Repo to Repo Collaboration](/images/tutorials/getting-started/setting-up-a-repository/02.svg)

Of course, there’s nothing stopping you from giving certain Git repos special meaning. For example, by simply designating one Git repo as the “central” repository, it’s possible to replicate a [Centralized workflow](/tutorials/comparing-workflows/centralized-workflow) using Git. The point is, this is accomplished through conventions rather than being hardwired into the VCS itself.

### Example

The example below demonstrates how to obtain a local copy of a central repository stored on a server accessible at `example.com` using the SSH username `john:`

```
git clone ssh://john@example.com/path/to/my-project.git 
cd my-project
# Start working on the project
```

The first command initializes a new Git repository in the `my-project` folder on your local machine and populates it with the contents of the central repository. Then, you can `cd` into the project and start editing files, committing snapshots, and interacting with other repositories. Also note that the `.git` extension is omitted from the cloned repository. This reflects the non-bare status of the local copy.
