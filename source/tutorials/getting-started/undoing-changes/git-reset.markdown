If `git revert` is a “safe” way to undo changes, you can think of `git reset` as the _dangerous_ method. When you undo with `git reset`(and the commits are no longer referenced by any ref or the reflog), there is no way to retrieve the original copy—it is a _permanent_ undo. Care must be taken when using this tool, as it’s one of the only Git commands that has the potential to lose your work.

Like [`git checkout`](/tutorials/undoing-changes/git-checkout), `git reset` is a versatile command with many configurations. It can be used to remove committed snapshots, although it’s more often used to undo changes in the staging area and the working directory. In either case, it should only be used to undo _local_ changes—you should never reset snapshots that have been shared with other developers.

### Usage

```
git reset <file>
```

Remove the specified file from the staging area, but leave the working directory unchanged. This unstages a file without overwriting any changes.

```
git reset
```

Reset the staging area to match the most recent commit, but leave the working directory unchanged. This unstages _all_ files without overwriting any changes, giving you the opportunity to re-build the staged snapshot from scratch.

```
git reset --hard
```

Reset the staging area and the working directory to match the most recent commit. In addition to unstaging changes, the `--hard` flag tells Git to overwrite all changes in the working directory, too. Put another way: this _obliterates_ all uncommitted changes, so make sure you really want to throw away your local developments before using it.

```
git reset <commit>
```

Move the current branch tip backward to `<commit>`, reset the staging area to match, but leave the working directory alone. All changes made since `<commit>` will reside in the working directory, which lets you re-commit the project history using cleaner, more atomic snapshots.

```
git reset --hard <commit>
```

Move the current branch tip backward to `<commit>` and reset both the staging area and the working directory to match. This obliterates not only the uncommitted changes, but all commits after `<commit>`, as well.

### Discussion

All of the above invocations are used to remove changes from a repository. Without the `--hard` flag, `git reset` is a way to clean up a repository by unstaging changes or uncommitting a series of snapshots and re-building them from scratch. The `--hard` flag comes in handy when an experiment has gone horribly wrong and you need a clean slate to work with.

Whereas reverting is designed to safely undo a _public_ commit, `git reset` is designed to undo _local_ changes. Because of their distinct goals, the two commands are implemented differently: resetting completely removes a changeset, whereas [reverting](/tutorials/undoing-changes/git-revert) maintains the original changeset and uses a new commit to apply the undo.

![Git Tutorial: Revert vs Reset](/images/tutorials/getting-started/undoing-changes/06.svg)

#### Don’t Reset Public History

You should never use `git reset <commit>` when any snapshots after <commit> have been pushed to a public repository. After publishing a commit, you have to assume that other developers are reliant upon it.

Removing a commit that other team members have continued developing poses serious problems for collaboration. When they try to sync up with your repository, it will look like a chunk of the project history abruptly disappeared. The sequence below demonstrates what happens when you try to reset a public commit. The `origin/master` branch is the central repository’s version of your local `master` branch.

![Git Tutorial: Resetting an Public Commit](/images/tutorials/getting-started/undoing-changes/07.svg)

As soon as you add new commits after the reset, Git will think that your local history has diverged from `origin/master`, and the merge commit required to synchronize your repositories is likely to confuse and frustrate your team.

The point is, make sure that you’re using `git reset <commit>` on a local experiment that went wrong—not on published changes. If you need to fix a public commit, the `git revert` command was designed specifically for this purpose.

### Examples

#### Unstaging a File

The `git reset` command is frequently encountered while preparing the staged snapshot. The next example assumes you have two files called `hello.py` and `main.py` that you’ve already added to the repository.

```
# Edit both hello.py and main.py

# Stage everything in the current directory
git add .

# Realize that the changes in hello.py and main.py
# should be committed in different snapshots

# Unstage main.py
git reset main.py

# Commit only hello.py
git commit -m "Make some changes to hello.py"

# Commit main.py in a separate snapshot
git add main.py
git commit -m "Edit main.py"
```

As you can see, `git reset` helps you keep your commits highly-focused by letting you unstage changes that aren’t related to the next commit.

#### Removing Local Commits

The next example shows a more advanced use case. It demonstrates what happens when you’ve been working on a new experiment for a while, but decide to completely throw it away after committing a few snapshots.

```
# Create a new file called `foo.py` and add some code to it

# Commit it to the project history
git add foo.py
git commit -m "Start developing a crazy feature"

# Edit `foo.py` again and change some other tracked files, too

# Commit another snapshot
git commit -a -m "Continue my crazy feature"

# Decide to scrap the feature and remove the associated commits
git reset --hard HEAD~2
```

The `git reset HEAD~2` command moves the current branch backward by two commits, effectively removing the two snapshots we just created from the project history. Remember that this kind of reset should only be used on _unpublished_ commits. Never perform the above operation if you’ve already pushed your commits to a shared repository.
