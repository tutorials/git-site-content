Git keeps track of updates to the tip of branches using a mechanism called reflog. This allows you to go back to changesets even though they are not referenced by any branch or tag. After rewriting history, the reflog contains information about the old state of branches and allows you to go back to that state if necessary.

### Usage

```
git reflog
```

Show the reflog for the local repository.

```
git reflog --relative-date
```

Show the reflog with relative date information (e.g. 2 weeks ago).

### Discussion

Every time the current HEAD gets updated (by switching branches, pulling in new changes, rewriting history or simply by adding new commits) a new entry will be added to the reflog.

### Example

To understand `git reflog`, let's run through an example.

```
0a2e358 HEAD@{0}: reset: moving to HEAD~2
0254ea7 HEAD@{1}: checkout: moving from 2.2 to master
c10f740 HEAD@{2}: checkout: moving from master to 2.2
```

The reflog above shows a checkout from master to the 2.2 branch and back. From there, there's a hard reset to an older commit. The latest activity is represented at the top labeled `HEAD@{0}`.

If it turns out that you accidentially moved back, the reflog will contain the commit master pointed to (0254ea7) before you accidentially dropped 2 commits.

```
git reset --hard 0254ea7
```

Using [`git reset`](/tutorials/undoing-changes/git-reset) it is then possible to change master back to the commit it was before. This provides a safety net in case history was accidentially changed.

It's important to note that the reflog only provides a safety net if changes have been commited to your local repository and that it only tracks movements.
