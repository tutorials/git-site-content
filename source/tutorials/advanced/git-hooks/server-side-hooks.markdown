Server-side hooks work just like local ones, except they reside in server-side repositories (e.g., a central repository, or a developer’s public repository). When attached to the official repository, some of these can serve as a way to enforce policy by rejecting certain commits.

There are 3 server-side hooks that we’ll be discussing in the rest of this article:

*   `pre-receive`
*   `update`
*   `post-receive`

All of these hooks let you react to different stages of the `git push` process.

The output from server-side hooks are piped to the client’s console, so it’s very easy to send messages back to the developer. But, you should also keep in mind that these scripts don’t return control of the terminal until they finish executing, so you should be careful about performing long-running operations.

### Pre-Receive

The `pre-receive` hook is executed every time somebody uses `git push` to push commits to the repository. It should always reside in the _remote_ repository that is the destination of the push, not in the originating repository.

The hook runs before any references are updated, so it’s a good place to enforce any kind of development policy that you want. If you don’t like who is doing the pushing, how the commit message is formatted, or the changes contained in the commit, you can simply reject it. While you can’t stop developers from making malformed commits, you can prevent these commits from entering the official codebase by rejecting them with `pre-receive`.

The script takes no parameters, but each ref that is being pushed is passed to the script on a separate line on standard input in the following format:

```
<old-value> <new-value> <ref-name>
```

You can see how this hook works using a very basic `pre-receive` script that simply reads in the pushed refs and prints them out.

```
#!/usr/bin/env python

import sys
import fileinput

# Read in each ref that the user is trying to update
for line in fileinput.input():
    print "pre-receive: Trying to push ref: %s" % line

# Abort the push
# sys.exit(1)
```

Again, this is a little different than the other hooks because information is passed to the script via standard input instead of as command-line arguments. After placing the above script in the `.git/hooks` directory of a remote repository and pushing the `master` branch, you’ll see something like the following in your console:

```
b6b36c697eb2d24302f89aa22d9170dfe609855b 85baa88c22b52ddd24d71f05db31f4e46d579095 refs/heads/master
```

You can use these SHA1 hashes, along with some lower-level Git commands, to inspect the changes that are going to be introduced. Some common use cases include:

*   Rejecting changes that involve an upstream rebase
*   Preventing non-fast-forward merges
*   Checking that the user has the correct permissions to make the intended
changes (mostly used for centralized Git workflows)

If multiple refs are pushed, returning a non-zero status from `pre-receive` aborts _all_ of them. If you want to accept or reject branches on a case-by-case basis, you need to use the `update` hook instead.

### Update

The `update` hook is called after `pre-receive`, and it works much the same way. It’s still called before anything is actually updated, but it’s called separately for each ref that was pushed. That means if the user tries to push 4 branches, `update` is executed 4 times. Unlike `pre-receive`, this hook doesn’t need to read from standard input. Instead, it accepts the following 3 arguments:

1.  The name of the ref being updated
2.  The old object name stored in the ref
3.  The new object name stored in the ref

This is the same information passed to `pre-receive`, but since `update` is invoked separately for each ref, you can reject some refs while allowing others.

```
#!/usr/bin/env python

import sys

branch = sys.argv[1]
old_commit = sys.argv[2]
new_commit = sys.argv[3]

print "Moving '%s' from %s to %s" % (branch, old_commit, new_commit)

# Abort pushing only this branch
# sys.exit(1)
```

The above `update` hook simply outputs the branch and the old/new commit hashes. When pushing more than one branch to the remote repository, you’ll see the `print` statement execute for each branch.

### Post-Receive

The `post-receive` hook gets called after a successful push operation, making it a good place to perform notifications. For many workflows, this is a better place to trigger notifications than `post-commit` because the changes are available on a public server instead of residing only on the user’s local machine. Emailing other developers and triggering a continuous integration system are common use cases for `post-receive`.

The script takes no parameters, but is sent the same information as `pre-receive` via standard input.
