Git hooks are scripts that run automatically every time a particular event
occurs in a Git repository. They let you customize Git’s internal
behavior and trigger customizable actions at key points in the development life
cycle.

![Hooks executing during the commit creation process](/images/tutorials/advanced/git-hooks/01.svg)

Common use cases for Git hooks include encouraging a commit policy, altering
the project environment depending on the state of the repository, and
implementing continuous integration workflows. But, since scripts are
infinitely customizable, you can use Git hooks to automate or optimize
virtually any aspect of your development workflow.

In this article, we’ll start with a conceptual overview of how Git
hooks work. Then, we’ll survey some of the most popular hooks for use in
both local and server-side repositories.