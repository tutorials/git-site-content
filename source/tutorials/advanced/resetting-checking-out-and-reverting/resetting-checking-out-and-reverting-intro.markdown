The `git reset`, `git checkout`, and `git
revert` command are some of the most useful tools in your Git toolbox.
They all let you undo some kind of change in your repository, and the first two
commands can be used to manipulate either commits or individual files.

Because they’re so similar, it’s very easy to mix up which
command should be used in any given development scenario. In this article,
we’ll compare the most common configurations of `git reset`,
`git checkout`, and `git revert`.  Hopefully,
you’ll walk away with the confidence to navigate your repository using
any of these commands.

![The main components of a Git repository](/images/tutorials/advanced/resetting-checking-out-and-reverting/01.svg)

It helps to think about each command in terms of their effect on the three
main components of a Git repository: the working directory, the staged
snapshot, and the commit history. Keep these components in mind as you read
through this article.