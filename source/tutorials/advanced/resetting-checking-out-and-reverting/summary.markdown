You should now have all the tools you could ever need to undo changes in a
Git repository. The `git reset`, `git checkout`, and
`git revert` commands can be confusing, but when you think about
their effects on the working directory, staged snapshot, and commit history, it
should be easier to discern which command fits the development task at
hand.

The table below sums up the most common use cases for all of these commands.
Be sure to keep this reference handy, as you’ll undoubtedly need to use
at least some them during your Git career.

| Command        | Scope        | Common use cases                                                    |
|----------------|--------------|---------------------------------------------------------------------|
| `git reset`    | Commit-level | Discard commits in a private branch or throw away uncommited changes|
| `git reset`    | File-level   | Unstage a file                                                      |
| `git checkout` | Commit-level | Switch between branches or inspect old snapshots                    |
| `git checkout` | File-level   | Discard changes in the working directory                            |
| `git revert`   | Commit-level | Undo commits in a public branch                                     |
| `git revert`   | File-level   | (N/A)                                                               |
