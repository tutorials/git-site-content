You should now be fairly comfortable using `git log`’s advanced parameters to format its output and select which commits you want to display. This gives you the power to pull out exactly what you need from your project history.

These new skills are an important part of your Git toolkit, but remember that `git log` is often used in conjunction other Git commands. Once you’ve found the commit you’re looking for, you typically pass it off to `git checkout`, `git revert`, or some other tool for manipulating your commit history. So, be sure to keep on learning about Git’s advanced features.
