The example below demonstrates how pull requests can be used in the Forking Workflow. It is equally applicable to developers working in small teams and to a third-party developer contributing to an open source project.

In the example, Mary is a developer, and John is the project maintainer. Both of them have their own public Bitbucket repositories, and John’s contains the official project.

### Mary forks the official project

![Pull Requests: Fork the project](/images/tutorials/collaborating/making-a-pull-request/09.svg)

To start working in the project, Mary first needs to fork John’s Bitbucket repository. She can do this by signing in to Bitbucket, navigating to John’s repository, and clicking the _Fork_ button.

![Pull Request: Fork in Bitbucket](/images/tutorials/collaborating/making-a-pull-request/10.svg)

After filling out the name and description for the forked repository, she will have a server-side copy of the project.

### Mary clones her Bitbucket repository

![Pull Request: Clone the Bitbucket repo](/images/tutorials/collaborating/making-a-pull-request/11.svg)

Next, Mary needs to clone the Bitbucket repository that she just forked. This will give her a working copy of the project on her local machine. She can do this by running the following command:

```
git clone https://user@bitbucket.org/user/repo.git
```

Keep in mind that `git clone` automatically creates an `origin` remote that points back to Mary’s forked repository.

### Mary develops a new feature

![Pull Requests: develop a new feature](/images/tutorials/collaborating/making-a-pull-request/12.svg)

Before she starts writing any code, Mary needs to create a new branch for the feature. This branch is what she will use as the source branch of the pull request.

```
git checkout -b some-feature
# Edit some code
git commit -a -m "Add first draft of some feature"
```

Mary can use as many commits as she needs to create the feature. And, if the feature’s history is messier than she would like, she can use an [interactive rebase](/tutorials/rewriting-history/git-rebase-i) to remove or squash unnecessary commits. For larger projects, cleaning up a feature’s history makes it much easier for the project maintainer to see what’s going on in the pull request.

### Mary pushes the feature to her Bitbucket repository

![Pull Requests: Push feature to Bitbucket repository](/images/tutorials/collaborating/making-a-pull-request/13.svg)

After her feature is complete, Mary pushes the feature branch to her own Bitbucket repository (not the official repository) with a simple `git push`:

```
git push origin some-branch
```

This makes her changes available to the project maintainer (or any collaborators who might need access to them).

### Mary creates the pull request

![Pull Request: Create Pull Request](/images/tutorials/collaborating/making-a-pull-request/14.svg)

After Bitbucket has her feature branch, Mary can create the pull request through her Bitbucket account by navigating to her forked repository and clicking the _Pull request_ button in the top-right corner. The resulting form automatically sets Mary’s repository as the source repository, and it asks her to specify the source branch, the destination repository, and the destination branch.

Mary wants to merge her feature into the main codebase, so the source branch is her feature branch, the destination repository is John’s public repository, and the destination branch is `master`. She’ll also need to provide a title and description for the pull request. If there are other people who need to approve the code besides John, she can enter them in the _Reviewers_ field.

![Pull Request: Bitbucket](/images/tutorials/collaborating/making-a-pull-request/pull-request-7.png)

After she creates the pull request, a notification will be sent to John via his Bitbucket feed and (optionally) via email.

### John reviews the pull request

![Pull Request: Bitbucket pull requests](/images/tutorials/collaborating/making-a-pull-request/pull-request-8.png)

John can access all of the pull requests people have filed by clicking on the _Pull request_ tab in his own Bitbucket repository. Clicking on Mary’s pull request will show him a description of the pull request, the feature’s commit history, and a diff of all the changes it contains.

If he thinks the feature is ready to merge into the project, all he has to do is hit the _Merge_ button to approve the pull request and merge Mary’s feature into his `master` branch.

But, for this example, let’s say John found a small bug in Mary’s code, and needs her to fix it before merging it in. He can either post a comment to the pull request as a whole, or he can select a specific commit in the feature’s history to comment on.

![Pull Request: Comment](/images/tutorials/collaborating/making-a-pull-request/pull-request-9.png)

### Mary adds a follow-up commit

If Mary has any questions about the feedback, she can respond inside of the pull request, treating it as a discussion forum for her feature.

To correct the error, Mary adds another commit to her feature branch and pushes it to her Bitbucket repository, just like she did the first time around. This commit is automatically added to the original pull request, and John can review the changes again, right next to his original comment.

### John accepts the pull request

Finally, John accepts the changes, merges the feature branch into master, and closes the pull request. The feature is now integrated into the project, and any other developers working on it can pull it into their own local repositories using the standard `git pull` command.
